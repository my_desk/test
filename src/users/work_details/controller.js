const { Users, Work_details } = require('../../../database/models')
const { FormValidation } = require('../../utils/formValidations')
const { converDataToLowerCAse } = require('../../utils/converData');
const { condition } = require('sequelize');

class WorkDetailsController {
  #detailsNeeded = ["position", "email", "employmentStatus", "userId"];

  findOneWorkDetails = async (conditions) => {
    return await Work_details.findOne({ where:conditions })
  }

  addWorkDetails = async (data) => {
    const values = converDataToLowerCAse(data) 
    const workDetailsForm = new FormValidation()
    const errors = workDetailsForm.validateInformation(values, this.#detailsNeeded)
    
    if (errors.length) {
      return { errors }
    }

    try {
      if(await this.findOneWorkDetails({ userId: data.userId })) {
        return { errors: [{user : 'Work Details for the user already exist'}] }
      }

      await Work_details.create(values)
      return { message: "Work details added", errors }
    } catch (error) {
      return { message: error.message }
    }
  }

  updateWorkDetails = async (conditions, data) => {
    const values = converDataToLowerCAse(data) 
    const workDetailsForm = new FormValidation()
    const updateDetailsNeeded = this.#detailsNeeded.filter(e => e !== 'userId'); 
    const errors = workDetailsForm.validateInformation(values, updateDetailsNeeded)
    
    if (errors.length) {
      return { errors }
    }

    try {
      if(!await this.findOneWorkDetails(conditions)) {
        return { errors: [{user : "Work Details doesn't exist"}] }
      }

      await Work_details.update(values, { where: conditions })
      return { message: "Work details updated", errors }
    } catch (error) {
      return { message: error.message }
    }
  }
}

module.exports = WorkDetailsController