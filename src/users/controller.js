const { Users, Work_details } = require('../../database/models')
const { FormValidation } = require('../utils/formValidations')
const { converDataToLowerCAse } = require('../utils/converData')
const { Op } = require("sequelize");

class UserController {
  #detailsNeeded = ["name", "age", "email", "gender", "address"];

  getAllUsers = async (req, res) => {
    try {
      res.send(
        await Users.findAll({
          include: [
            {
              model: Work_details,
              as: 'work_details'
            }
          ]
        })
      )
    } catch (error) {
      res.send({ message: error.message }) 
    }
  }

  getOneUser = async (conditions) => {
    return await Users.findOne({ where: conditions });
  }

  addUser = async (req, res) => {
    const values = converDataToLowerCAse(req.body)
    const userForm = new FormValidation()
    const errors = userForm.validateInformation(values, this.#detailsNeeded)
    
    if (errors.length) {
      res.send({ errors })
    }

    try {
      if (await this.getOneUser({ email: data.email })) {
        res.send({ errors: [{ email: "Email is already used" }] })
      }
      
      await Users.create(values)
      res.send({ message: "User Created", errors })
    } catch (error) {
      res.send({ message: error.message })
    }
  }

  updateUser = async (conditions, data) => {
    const values = converDataToLowerCAse(data)
    const userForm = new FormValidation()
    const errors = userForm.validateInformation(values, this.#detailsNeeded)
    
    if (errors.length) {
      return { errors }
    }

    try {
      if (!await this.getOneUser(conditions)) {
        return { errors: [{ user: "User doesn't exist"}] }
      }

      const getUserConditions = {
        [Op.and]: [
          { email: data.email },
          { id: { [Op.ne]: conditions.id } }
        ]
      }

      if (await this.getOneUser(getUserConditions)) {
        return { errors: [{ email: "Email is already used" }] }
      }

      await Users.update(
        values,
        {
          where: conditions
        }
      )

      return { message: "Updated User" }
    } catch (error) {
      return { message: error.message }
    }
  }

  deleteUser = async (conditions) => {
    try {
      if (!await this.getOneUser(conditions)) {
        return { message: "User doesn't exist" }
      }

      await Users.destroy({where: conditions})
      return { message: 'Deleted User' }
    } catch (error) {
      return { message: error.message }
    }
  }
}

module.exports = UserController