const express = require('express');
const UserController = require('./controller');
const WorkDetailsController = require('./work_details/controller');

class UserRoutes {
  #userController = new UserController()
  #workDetailsController = new WorkDetailsController
  router = express.Router();
  #path = ''
  #workDetailsPath = '/work-details'

  constructor() {
    this.#initializeRoutes();
  }

  #initializeRoutes() {
    // router for user details
    this.router.get(`${this.#path}`, this.#userController.getAllUsers)

    this.router.get(`${this.#path}/:id`, async (req, res) => {
      res.send(await this.#userController.getOneUser({id : req.params.id }))
    })

    this.router.post(`${this.#path}`, this.#userController.addUser)

    this.router.put(`${this.#path}/:id`, async (req, res) => {
      res.send(await this.#userController.updateUser({id : req.params.id }, req.body))
    })

    this.router.delete(`${this.#path}/:id`, async (req, res) => {
      res.send(await this.#userController.deleteUser({id : req.params.id }, req.body))
    })

    // router for work details
    this.router.post(`${this.#workDetailsPath}`, async (req, res) => {
      res.send(await this.#workDetailsController.addWorkDetails(req.body))
    })

    this.router.put(`${this.#workDetailsPath}/:id`, async (req, res) => {
      res.send(await this.#workDetailsController.updateWorkDetails({ userId : req.params.id }, req.body))
    })
  }
}

module.exports = new UserRoutes()