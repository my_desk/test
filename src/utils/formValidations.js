class FormValidation { 
  #errors = [];
  #genders = ["male", "female"];

  validateInformation = (data, detailsNeeded) => {
    this.checkIfNull(data, detailsNeeded)

    if (data.email && !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(data.email)) {
      this.#errors.push({ email: "incorrect email format" });
    }

    if (data.gender && !this.checkIfGenderExist(data.gender)) {
      this.#errors.push({ gender: "gender doesn't exist" });
    }

    return this.#errors
  }

  checkIfGenderExist = (value) => {
    return this.#genders.find(gender => (value === gender));
  }

  checkIfNull = (values, detailsNeeded) => {
    detailsNeeded.map(data => {
      if (!values[data]) {
        this.#errors.push({ [data]: `${data} is required` })
      }
    })
  }
}

module.exports = { FormValidation }