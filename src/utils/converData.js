const converDataToLowerCAse = (data) => {
  let values = {}

  Object.keys(data).map(key => {
    values = {
      ...values,
      [key] : typeof data[key] === 'string' ? data[key].toLowerCase() : data[key]
    }
  })

  return values
}

module.exports = { converDataToLowerCAse }