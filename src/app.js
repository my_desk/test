const UserRoutes = require('./users/routes') 
const Modules = require('./modules')
const dotenv = require('dotenv')

dotenv.config()

const port = 5000
var corsOptions = {
  origin: `http://localhost:${port}`
};

const app = new Modules(
  [
    UserRoutes,
  ],
  port,
  corsOptions
);

app.listen()
