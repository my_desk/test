'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasOne(models.Work_details, { foreignKey: 'userId', as: 'work_details' })
    }
  }
  Users.init({
    name: DataTypes.STRING,
    age: DataTypes.STRING,
    email: DataTypes.STRING,
    gender: DataTypes.ENUM('male','female'),
    address: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Users',
  });
  return Users;
};