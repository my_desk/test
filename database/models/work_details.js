'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Work_details extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Users, { as: 'user' })
    }
  }
  Work_details.init({
    position: DataTypes.STRING,
    email: DataTypes.STRING,
    employmentStatus: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Work_details',
  });
  return Work_details;
};